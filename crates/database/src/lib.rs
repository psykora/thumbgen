use chrono::DateTime;
use console::style;
use opt::DatabaseOpts;
use sqlx::sqlite::{SqliteConnectOptions, SqliteJournalMode, SqlitePoolOptions, SqliteSynchronous};
use sqlx::{Error, Pool, Sqlite};
use std::path::PathBuf;
use std::str::FromStr;
use std::time::Duration;

const POOL_TIMEOUT: Duration = Duration::from_secs(30);
const POOL_MAX_CONNECTIONS: u32 = 1;

pub async fn create(database_opts: &DatabaseOpts) -> anyhow::Result<()> {
    let sqlite_pool = sqlite_connect(database_opts, true).await?;

    sqlx::query("pragma temp_store = memory;")
        .execute(&sqlite_pool)
        .await?;

    let sql1 = r#"
    create table dirs (
        id integer primary key,
        path text not null unique,
        created_at integer not null default (unixepoch())
    );"#;

    sqlx::query(sql1).execute(&sqlite_pool).await?;

    let sql2 = r#"
    create table images (
        id integer primary key,
        alias text not null unique,   -- using ULID; also the name for a WebP thumbnail
        dir_id integer not null references dirs(id),
        name text not null, -- file name
        exif text_json,
        created_at integer not null default (unixepoch())
    );"#;

    sqlx::query(sql2).execute(&sqlite_pool).await?;

    let _ = &sqlite_pool.close().await;

    Ok(())
}

#[allow(unused)]
#[derive(Debug, sqlx::FromRow)]
struct ImgDir {
    id: i64,
    path: String,
    created_at: DateTime<chrono::Utc>,
}

pub async fn list_image_dirs(database_opts: &DatabaseOpts) -> anyhow::Result<()> {
    let sqlite_pool = sqlite_connect(database_opts, false).await?;

    let dirs = sqlx::query_as::<_, ImgDir>("SELECT id, path, created_at from dirs")
        .fetch_all(&sqlite_pool)
        .await?;

    dirs.iter().for_each(|dir| println!("{:?}", dir));

    let _ = &sqlite_pool.close().await;

    println!(
        "{} {}",
        style("Total count of image dirs: ").yellow(),
        dirs.len()
    );
    Ok(())
}

pub async fn add_image_dir(conn_pool: &Pool<Sqlite>, image_dir: &str) -> anyhow::Result<i64> {
    let (id,): (i64,) = sqlx::query_as("INSERT INTO dirs (path) VALUES ($1) RETURNING id")
        .bind(&image_dir)
        .fetch_one(conn_pool)
        .await?;

    Ok(id)
}

pub async fn add_image_file(
    conn_pool: &Pool<Sqlite>,
    image_dir_id: i64,
    alias: &str,
    image_file_name: &str,
) -> anyhow::Result<i64> {
    let (id,): (i64,) =
        sqlx::query_as("INSERT INTO images (alias, dir_id, name) VALUES ($1, $2, $3) RETURNING id")
            .bind(&alias)
            .bind(image_dir_id)
            .bind(&image_file_name)
            .fetch_one(conn_pool)
            .await?;

    Ok(id)
}

pub async fn get_image_dir_id(conn_pool: &Pool<Sqlite>, image_dir: &str) -> anyhow::Result<i64> {
    let (id,): (i64,) = sqlx::query_as("SELECT id from dirs where path = $1")
        .bind(&image_dir)
        .fetch_one(conn_pool)
        .await?;

    Ok(id)
}

pub async fn list_aliases(
    conn_pool: &Pool<Sqlite>,
    image_dir_id: i64,
) -> anyhow::Result<Vec<String>> {
    let aliases: Vec<String> = sqlx::query_as("SELECT alias from images where dir_id = $1")
        .bind(image_dir_id)
        .fetch_all(conn_pool)
        .await?
        .into_iter()
        .map(|r: (String,)| r.0)
        .collect();

    Ok(aliases)
}

pub async fn delete_aliases_and_dir(
    conn_pool: &Pool<Sqlite>,
    image_dir_id: i64,
) -> anyhow::Result<()> {
    let mut tx = conn_pool.begin().await?;

    sqlx::query("DELETE FROM IMAGES WHERE dir_id = $1")
        .bind(image_dir_id)
        .execute(&mut tx)
        .await?;

    sqlx::query("DELETE FROM DIRS WHERE id = $1")
        .bind(image_dir_id)
        .execute(&mut tx)
        .await?;

    tx.commit().await?;

    Ok(())
}

/// Creates an SQLite database connections pool
pub async fn sqlite_connect(
    database_opts: &DatabaseOpts,
    create: bool,
) -> anyhow::Result<Pool<Sqlite>, Error> {
    let database_file = String::from(db_file_full(database_opts).to_string_lossy());
    let database_url = db_url(&database_file);

    let connection_options = SqliteConnectOptions::from_str(database_url.as_str())?
        .create_if_missing(create)
        .journal_mode(SqliteJournalMode::Wal)
        .synchronous(SqliteSynchronous::Normal)
        .busy_timeout(POOL_TIMEOUT);

    SqlitePoolOptions::new()
        .max_connections(POOL_MAX_CONNECTIONS)
        .acquire_timeout(POOL_TIMEOUT)
        .connect_with(connection_options)
        .await
}

/// returns the full name of the database file
fn db_file_full(database_opts: &DatabaseOpts) -> PathBuf {
    let mut path = PathBuf::from(&database_opts.database_dir);
    path.push(&database_opts.database_file);
    path
}

/// returns the database URL for sqlx
fn db_url(full_name: &str) -> String {
    format!("sqlite://{}", full_name)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[cfg(target_family = "windows")]
    fn test_db_file_full() {
        let database_opts = DatabaseOpts {
            database_dir: String::from(r"c:\abc\cde\"),
            database_file: String::from("db.sqlite"),
            wal: true,
        };

        let full_path = db_file_full(&database_opts);
        assert_eq!(full_path, PathBuf::from(r"c:\abc\cde\db.sqlite"))
    }

    #[test]
    #[cfg(target_family = "unix")]
    fn test_db_file_full() {
        let database_opts = DatabaseOpts {
            database_dir: String::from("/abc/cde"),
            database_file: String::from("db.sqlite"),
            wal: true,
        };

        let full_path = db_file_full(&database_opts);
        assert_eq!(full_path, PathBuf::from("/abc/cde/db.sqlite"))
    }

    #[test]
    fn test_db_url() {
        let db_url = db_url("/abc/cde/db.sqlite");
        assert_eq!(db_url, "sqlite:///abc/cde/db.sqlite")
    }
}
