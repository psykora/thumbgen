mod home;
mod folders;

use axum::routing::get;
use axum::Router;

use home::home;
use folders::folders;

pub fn create_routes() -> Router {
    Router::new()
        .route("/", get(home))
        .route("/folders", get(folders))
}