use askama::Template;
use axum::http::StatusCode;
use axum::response::{Html, IntoResponse, Response};

pub async fn folders() -> impl IntoResponse {
    let dirs = vec![Dir {id: 1, path: "abc".to_owned()}, Dir {id: 2, path: "/abc/cde".to_owned()}];
    let template = FoldersTemplate { dirs };
    HtmlTemplate(template)
}

#[derive(Template)]
#[template(path = "folders.html")]
struct FoldersTemplate {
    dirs: Vec<Dir>,
}

struct Dir {
    id: i64,
    path: String,
}

struct HtmlTemplate<T>(T);

impl<T> IntoResponse for HtmlTemplate<T>
    where
        T: Template,
{
    fn into_response(self) -> Response {
        match self.0.render() {
            Ok(html) => Html(html).into_response(),
            Err(err) => (
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("Failed to render template. Error: {}", err),
            )
                .into_response(),
        }
    }
}