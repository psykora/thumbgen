use axum::response::Html;

pub async fn home() -> Html<&'static str> {
    Html(r#"
        <h1>Thumbnail Viewer</h1>
        <p>
            <a href="/folders">Seznam adresářů</a>
        </p>
    "#)
}
