mod routes;

use std::net::SocketAddr;
use routes::create_routes;

pub async fn run() {
    let app = create_routes();

    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));
    println!("listening on {}", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}
