use clap::{Args, Parser};

#[derive(Parser, Debug)]
#[command(author, version, about)]
#[command(propagate_version = true)]
pub struct Opt {
    #[command(subcommand)]
    pub command: Command,
}

#[derive(Parser, Debug)]
pub enum Command {
    /// Creates and initializes the database and the thumbnail directory.
    SetUp {
        #[clap(flatten)]
        database_opts: DatabaseOpts,

        #[clap(flatten)]
        thumbnail_dir_opts: ThumbnailDirOpts,
    },

    ///  Deletes all thumbnails and the database.
    TearDown {
        #[clap(flatten)]
        confirmation: Confirmation,

        #[clap(flatten)]
        database_opts: DatabaseOpts,

        #[clap(flatten)]
        thumbnail_dir_opts: ThumbnailDirOpts,
    },

    /// Lists the photos/images directories from the database.
    List {
        #[clap(flatten)]
        database_opts: DatabaseOpts,
    },

    /// Add a directory to the DB and process the photos/images in it.
    Add {
        #[clap(flatten)]
        database_opts: DatabaseOpts,

        #[clap(flatten)]
        thumbnail_dir_opts: ThumbnailDirOpts,

        #[clap(flatten)]
        image_dir_opts: ImageDirOpts,
    },

    /// Remove the photo/image directory from the DB and deletes the related thumbnails.
    Remove {
        #[clap(flatten)]
        database_opts: DatabaseOpts,

        #[clap(flatten)]
        thumbnail_dir_opts: ThumbnailDirOpts,

        #[clap(flatten)]
        image_dir_opts: ImageDirOpts,
    },
}

/// Arguments for the SQLite database.
#[derive(Args, Debug)]
pub struct DatabaseOpts {
    /// Full path of directory where the SQLite DB file(s) reside(s), by default will be read from the `DATABASE_DIR` env var
    #[clap(long, short = 'D', env, default_value = "/var/lib/thumbgen/db")]
    pub database_dir: String,

    /// SQLite DB file name, by default `images.sqlite`
    #[clap(long, short = 'f', default_value = "photodb.sqlite")]
    pub database_file: String,

    /// Set whether or not to create SQLite databases in Write-Ahead Log (WAL) mode:
    /// https://www.sqlite.org/wal.html
    ///
    /// WAL mode is enabled by default.
    ///
    /// However, if your application sets a `journal_mode` on `SqliteConnectOptions` to something
    /// other than `Wal`, then it will have to take the database file out of WAL mode on connecting,
    /// which requires an exclusive lock and may return a `database is locked` (`SQLITE_BUSY`) error.
    #[clap(long, action = clap::ArgAction::Set, default_value = "true")]
    pub wal: bool,
}

/// Argument for the image directory and thumbnail directory.
#[derive(Args, Debug)]
pub struct ThumbnailDirOpts {
    /// Full path of the directory for WebP thumbnails, by default will be read from the THUMBNAIL_DIR env var
    #[clap(long, short = 'T', env, default_value = "/var/lib/thumbgen/webp")]
    pub thumbnail_dir: String,
}

/// Argument for the image directory and thumbnail directory.
#[derive(Args, Debug)]
pub struct ImageDirOpts {
    /// Full path of directory with images
    #[clap(long, short = 'i')]
    pub image_dir: String,
}

/// Argument for automatic confirmation.
#[derive(Args, Copy, Clone, Debug)]
pub struct Confirmation {
    /// Automatic confirmation. Without this option, you will be prompted before dropping
    /// your database.
    #[clap(short)]
    pub yes: bool,
}
