use crate::directory::delete_directory;
use clap::Parser;
use console::style;
use opt::{Command, DatabaseOpts, ImageDirOpts, Opt, ThumbnailDirOpts};
use promptly::{prompt, ReadlineError};
use ulid::Ulid;

mod directory;
mod webp;

#[tokio::main]
async fn main() {
    dotenvy::dotenv().ok();
    // no special handling here
    if let Err(error) = run(Opt::parse()).await {
        println!("{} {}", style("error:").bold().red(), error);
        std::process::exit(1);
    }
}

async fn run(opt: Opt) -> anyhow::Result<()> {
    match opt.command {
        Command::SetUp {
            database_opts,
            thumbnail_dir_opts,
        } => set_up(&database_opts, &thumbnail_dir_opts).await?,

        Command::TearDown {
            confirmation,
            database_opts,
            thumbnail_dir_opts,
        } => tear_down(&database_opts, &thumbnail_dir_opts, !confirmation.yes).await?,

        Command::List { database_opts } => list(&database_opts).await?,

        Command::Add {
            database_opts,
            thumbnail_dir_opts,
            image_dir_opts,
        } => add(&database_opts, &thumbnail_dir_opts, &image_dir_opts).await?,

        Command::Remove {
            database_opts,
            thumbnail_dir_opts,
            image_dir_opts,
        } => remove(&database_opts, &thumbnail_dir_opts, &image_dir_opts).await?,
    };

    Ok(())
}

async fn set_up(
    database_opts: &DatabaseOpts,
    thumbnail_dir_opts: &ThumbnailDirOpts,
) -> anyhow::Result<()> {
    // check the database directory exists and is empty
    directory::directory_is_empty(&database_opts.database_dir).await?;

    // check the thumbnail directory exists and is empty
    directory::directory_is_empty(&thumbnail_dir_opts.thumbnail_dir).await?;

    // TODO: check the database directory is not the same as the thumbnail directory (why not??)

    // create and set up the database
    database::create(&database_opts).await?;

    println!("{}", style("Set-up finished").yellow());
    Ok(())
}

async fn tear_down(
    database_opts: &DatabaseOpts,
    thumbnail_dir_opts: &ThumbnailDirOpts,
    confirm: bool,
) -> anyhow::Result<()> {
    let database_dir = &database_opts.database_dir;
    let thumbnail_dir = &thumbnail_dir_opts.thumbnail_dir;

    if confirm && !ask_to_continue(database_dir, thumbnail_dir) {
        return Ok(());
    }

    match delete_directory(database_dir).await {
        Ok(()) => println!(
            "Database in directory {} deleted {}",
            style(database_dir).cyan(),
            style("successfully.").green()
        ),
        Err(e) => println!(
            "Deleting database in directory {} failed. Error: {}",
            style(database_dir).cyan(),
            style(e).red()
        ),
    };

    match delete_directory(thumbnail_dir).await {
        Ok(()) => println!(
            "Thumbnails in directory {} deleted {}",
            style(thumbnail_dir).cyan(),
            style("successfully.").green()
        ),
        Err(e) => println!(
            "Deleting thumbnails in directory {} failed. Error: {}",
            style(thumbnail_dir).cyan(),
            style(e).red()
        ),
    };

    println!("{}", style("Tear-down finished").yellow());
    Ok(())
}

async fn list(database_opts: &DatabaseOpts) -> anyhow::Result<()> {
    database::list_image_dirs(database_opts).await?;
    Ok(())
}

async fn add(
    database_opts: &DatabaseOpts,
    thumbnail_dir_opts: &ThumbnailDirOpts,
    image_dir_opts: &ImageDirOpts,
) -> anyhow::Result<()> {
    let paths = directory::get_image_files(&image_dir_opts.image_dir).await?;

    let db_connection = database::sqlite_connect(database_opts, false).await?;
    let dir_id = database::add_image_dir(&db_connection, &image_dir_opts.image_dir).await?;

    let msg = format!(
        "Image directory {} added do the database with ID={}",
        &image_dir_opts.image_dir, dir_id
    );
    println!("{}", style(msg).green());

    for path in paths {
        let file_name = path.file_name().unwrap().to_str().unwrap();
        let ulid = Ulid::new().to_string();
        print!("processing file: {} (alias = {}) ...", file_name, ulid);
        database::add_image_file(&db_connection, dir_id, &ulid, file_name).await?;
        webp::image_to_webp(&path, &thumbnail_dir_opts.thumbnail_dir, &ulid).await?;
        println!("done.")
    }

    let msg = format!("Image directory {} processed", &image_dir_opts.image_dir);
    println!("{}", style(msg).yellow());
    Ok(())
}

async fn remove(
    database_opts: &DatabaseOpts,
    thumbnail_dir_opts: &ThumbnailDirOpts,
    image_dir_opts: &ImageDirOpts,
) -> anyhow::Result<()> {
    let db_connection = database::sqlite_connect(database_opts, false).await?;

    let dir_id = database::get_image_dir_id(&db_connection, &image_dir_opts.image_dir).await?;
    println!("dir_id = {}", &dir_id);

    let aliases = database::list_aliases(&db_connection, dir_id).await?;
    println!("alias = {:?}", &aliases);

    database::delete_aliases_and_dir(&db_connection, dir_id).await?;
    directory::delete_thumbnails(&thumbnail_dir_opts.thumbnail_dir, &aliases).await?;

    let msg = format!(
        "Image directory {} removed from database and related thumbnails deleted.",
        &image_dir_opts.image_dir
    );
    println!("{}", style(msg).yellow());
    Ok(())
}

fn ask_to_continue(database_dir: &str, thumbnail_dir: &str) -> bool {
    loop {
        let r: Result<String, ReadlineError> = prompt(format!(
            "Delete directories {} (DB) and {} (thumbnails)? (y/n)",
            style(&database_dir).cyan(),
            style(&thumbnail_dir).cyan(),
        ));
        match r {
            Ok(response) => {
                if response == "n" || response == "N" {
                    return false;
                } else if response == "y" || response == "Y" {
                    return true;
                } else {
                    println!(
                        "Response not recognized: {}\nPlease type 'y' or 'n' and press enter.",
                        response
                    );
                }
            }
            Err(e) => {
                println!("{}", e);
                return false;
            }
        }
    }
}
