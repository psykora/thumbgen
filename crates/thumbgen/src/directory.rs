use anyhow::{bail, Context, Result};
use std::ffi::OsStr;
use std::path::{Path, PathBuf};
use tokio::fs;

/// Checks, if the directory exists and if it is empty
pub async fn directory_is_empty(dir: &str) -> Result<()> {
    let path = Path::new(dir);
    let is_empty = fs::read_dir(path)
        .await
        .with_context(|| format!("Directory not found or invalid: {}", dir))?
        .next_entry()
        .await
        .with_context(|| format!("Error in reading directory: {}", dir))?
        .is_none();

    if !is_empty {
        bail!("Directory is not empty: {}", dir);
    }

    Ok(())
}

/// Deletes the directory (including the content) and re-creates it
pub async fn delete_directory(dir: &str) -> Result<()> {
    fs::remove_dir_all(dir).await?;
    fs::create_dir(dir).await?;
    Ok(())
}

/// Deletes the thumbnails specified by aliases
pub async fn delete_thumbnails(thumbnail_dir: &str, aliases: &Vec<String>) -> Result<()> {
    for alias in aliases.iter() {
        let mut path = PathBuf::new();
        path.push(&thumbnail_dir);
        path.push(alias);
        path.set_extension("webp");
        fs::remove_file(&path).await?;
    }
    Ok(())
}

/// Returns image file paths from the directory
pub async fn get_image_files(dir: &str) -> Result<Vec<PathBuf>> {
    let mut files: Vec<PathBuf> = Vec::new();

    let mut entries = fs::read_dir(dir).await?;
    while let Some(entry) = entries.next_entry().await? {
        let extension = Path::new(
            entry
                .file_name()
                .to_str()
                .expect("Error in file name to str conversion"),
        )
        .extension()
        .and_then(OsStr::to_str)
        .map(|ext| ext.to_lowercase());
        if let Some(ext) = extension {
            if ext == "jpg" || ext == "jpeg" || ext == "png" || ext == "webp" {
                files.push(entry.path());
            }
        }
    }

    Ok(files)
}
