use anyhow::Result;
use image::io::Reader as ImageReader;
use webp::{Encoder, WebPMemory};

use std::path::PathBuf;
use tokio::fs;

/// Converts an image from any supported format to WEBP.
/// # Parameters
/// * `file_path`: Path to the image to convert.
/// * `thumbnail_dir`: Directory for the thumbnail.
/// * `ulid`: Generated name for the thumbnail (ULID).
pub async fn image_to_webp(file_path: &PathBuf, thumbnail_dir: &str, ulid: &str) -> Result<()> {
    // Open path as DynamicImage
    // let file_path = file_path;
    let image = ImageReader::open(file_path)?
        .with_guessed_format()?
        .decode()?
        .thumbnail(400, 400);

    // Make webp::Encoder from DynamicImage.
    let encoder = Encoder::from_image(&image).ok().unwrap();

    // Encode image into WebPMemory.
    let encoded_webp: WebPMemory = encoder.encode(40f32);

    // Put webp-image in a thumbnail directory.
    let mut path = PathBuf::from(thumbnail_dir);

    // Get filename of target.
    path.push(ulid);
    path.set_extension("webp");

    // Save webp image to file
    fs::write(&path, &*encoded_webp).await?;

    Ok(())
}

// Based on:
// see: https://users.rust-lang.org/t/converting-png-jpeg-image-to-webp/71080/3
// see: https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=940fa7301b56a6016a138930c1e6c32f
// see: https://blog.logrocket.com/decoding-encoding-images-rust-using-image-crate/
