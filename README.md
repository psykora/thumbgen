# ThumbGen

## Thumbnail Generator for Photo/Image Archives

A simple CLI utility indexing a directory containing photos/images, 
extracting metadata and creating WebP thumbnails.

It uses SQLite for saving directory info, file info and metadata (EXIF).

## Operations

### Set Up

The operation:
* Checks if the database directory exists and is empty
* Checks if the thumbnail directory exists and is empty
* Creates the database file in the database directory
* Initializes the database (creates tables etc.)

Examples:

Using CLI parameters (Linux shell):
```bash
thumbgen set-up --database-dir /var/thumbgen/db  --thumbnail-dir /var/thumbgen/webp 
```

Using environment variables (Linux shell):
```bash
export DATABASE_DIR=/var/thumbgen/db
export THUMBNAIL_DIR=/var/thumbgen/webp
thumbgen set-up 
```

Using environment variables (Windows powershell):
```powershell
$env:DATABASE_DIR="c:\thumbgen\db"
$env:THUMBNAIL_DIR="c:\thumbgen\webp"
thumbgen set-up 
```


### Tear Down

This operation must be confirmed.

The operation:
* Deletes the thumbnail directory (including all the content) and creates a new (empty) one 
* Deletes the database directory (including all the content) and creates a new (empty) one

#### Examples

Using CLI parameters (Linux shell):
```bash
thumbgen tear-down --database-dir /var/thumbgen/db  --thumbnail-dir /var/thumbgen/webp 
```

Using environment variables (Linux shell):
```bash
export DATABASE_DIR=/var/thumbgen/db
export THUMBNAIL_DIR=/var/thumbgen/webp
thumbgen tear-down 
```

Using environment variables (Windows powershell):
```powershell
$env:DATABASE_DIR="c:\thumbgen\db"
$env:THUMBNAIL_DIR="c:\thumbgen\webp"
thumbgen tear-down 
```


### List Photo Directories

The operation lists directory records from the database to the standard output.  

#### Examples

Using CLI parameters (Linux shell):
```bash
thumbgen list --database-dir /var/thumbgen/db  --thumbnail-dir /var/thumbgen/webp 
```

Using environment variables (Linux shell):
```bash
export DATABASE_DIR=/var/thumbgen/db
export THUMBNAIL_DIR=/var/thumbgen/webp
thumbgen list 
```

Using environment variables (Windows powershell):
```powershell
$env:DATABASE_DIR="c:\thumbgen\db"
$env:THUMBNAIL_DIR="c:\thumbgen\webp"
thumbgen list 
```

### Add a Photo/Image Directory

The operation:
* Checks if the photo/image directory exists
* Stores the directory record into the database
* Iterates through all the JPEG files in the directory and for each file
  * Creates a global alias (ULID)
  * Extracts EXIF metadata
  * Creates a file record in the database
  * Creates a WebP thumbnail and stores it in the thumbnail directory

#### Examples

Using CLI parameters (Linux shell):
```bash
thumbgen add --database-dir /var/thumbgen/db  --thumbnail-dir /var/thumbgen/webp --image-dir /photos/2022-12-13_New-York 
```

Using environment variables (Linux shell):
```bash
export DATABASE_DIR=/var/thumbgen/db
export THUMBNAIL_DIR=/var/thumbgen/webp
thumbgen add --image-dir /photos/2022-12-13_New-York 
```

Using environment variables (Windows powershell):
```powershell
$env:DATABASE_DIR="c:\thumbgen\db"
$env:THUMBNAIL_DIR="c:\thumbgen\webp"
thumbgen add --image-dir "c:\photos\2022-12-13_New-York" 
```

### Remove the Photo Directory

The operation:
* Checks if the directory record is in the database
* Iterates through the file database records and for each file 
    * Deletes the WebP thumbnail
    * Deletes the file record from the database
* Deletes the directory record from the database

NOTE: It does not touch the files in the photo/image directory.

#### Examples

Using CLI parameters (Linux shell):
```bash
thumbgen remove --database-dir /var/thumbgen/db  --thumbnail-dir /var/thumbgen/webp --image-dir /photos/2022-12-13_New-York 
```

Using environment variables (Linux shell):
```bash
export DATABASE_DIR=/var/thumbgen/db
export THUMBNAIL_DIR=/var/thumbgen/webp
thumbgen remove --image-dir /photos/2022-12-13_New-York 
```

Using environment variables (Windows powershell):
```powershell
$env:DATABASE_DIR="c:\thumbgen\db"
$env:THUMBNAIL_DIR="c:\thumbgen\webp"
thumbgen remove --archive-dir "c:\photos\2022-12-13_New-York" 
```

## CLI Options and Environment Variables

### Directory for the SQLite Database

Environment variable: `DATABASE_DIR`

CLI option: `--database-dir` or `-D` 

An absolute path to the database directory.
The CLI option overrides the environment variable.

### File for the SQLite Database

CLI option: `--database-file`

Optional. The default value is `photodb.sqlite`

A file name for the SQLite database. The file resides in the database directory.

### Directory for WebP Thumbnails

Environment variable: `THUMBNAIL_DIR`

CLI option: `--thumbnail-dir` or `-T` 

An absolute path to the thumbnails directory.
The CLI option overrides the environment variable.

### Directory with Photos/Images

CLI option: `--image-dir` or `-i`

An absolute or relative path to an existing directory with photos/images. 
Only files in this particular directory are processed. I.e. no recursive traversal.